%% ALL TROUBLES ORIGINATE IN FEATURE EXTRACTOR
clear all
[raw_database, extracted_database] = LoadDatabase();
classes = {'m'; '+'; '1'; '2'; '3'; '4'; '5'; '6'; '7'; '8'};
num_classes = size(classes,1);
index_classes = 1 : num_classes;
num_repeats =20;

%% Average classification error of our classifier
clc
test_to_train_ratio=0.25;
nStates = 3;
classification_error = zeros(index_classes(1, end), index_classes(1, end));
%k-fold val
k = 5;
num_repeat_test = round(num_repeats/k);
indices = randperm(num_repeats);
classi_derp = zeros(num_classes, num_repeat_test);
for i = 1:k
    training_indexes = indices;
    test_indexes = training_indexes(indices(1+(i-1)*num_repeat_test:i*num_repeat_test));
    training_indexes = setdiff(training_indexes,test_indexes);
    
%     Subsampling cross-validation
%     indices = randperm(num_repeats);
%     training_indexes = 1:num_repeats;
%     num_repeat_test = round(0.25*num_repeats);
%     test_indexes = training_indexes(indices(1:num_repeat_test));
%     training_indexes = training_indexes(indices(num_repeat_test+1:end));

    %Partition data into training and test set
    [training_set, test_set] = partition(extracted_database, index_classes, training_indexes, test_indexes);
    
    %Train data
    hmm=trainHMMS(training_set, index_classes);

    % Classify
    for class_index = index_classes
        for test_index = 1 : size(test_indexes, 2)
            [~, classified_index] = max(logprob(hmm, test_set{class_index, test_index}));
            %Classified as
            classified = classes{classified_index};
            %Correct class
            correct_class = classes{class_index};
            if classified ~= correct_class
                classification_error(class_index, classified_index) = classification_error(class_index, classified_index)+1;
%                 figure
%                 length = size(test_set{class_index, test_index},2);
%                 subplot(2, 1, 1);
%                 plot(1:length,test_set{class_index, test_index}(1,:));
%                 subplot(2, 1, 2);
%                 plot(1:length,test_set{class_index, test_index}(2,:));
%                 suptitle(classified)
                
            end
        end
    end
end
avgerr_missclass = sum(classification_error,2)/(k*num_repeat_test);
%avgerr = sum of errors / #permutations of sets * #classes * #test samples
avgerr = sum(sum(classification_error))/(k* num_classes * num_repeat_test);
%% Train on full dataset for Live example
[training_set, test_set] = partition(extracted_database, index_classes, 1:20, []);
hmm=trainHMMS(training_set, index_classes);
%hmm=trainHMMS(training_set, index_classes,3);
save('liveHMM', 'hmm')
%% Live-test
load('liveHMM', 'hmm')
clc
for i = 1:3 
    live_data =  DrawCharacter(5);
    [extracted_live{i}, ~] = ExtractFeaturesDistAng(live_data);
    [~, classifi_index{i}] = max(logprob(hmm, extracted_live{i}(1:2,:)));
    classified = classes{classifi_index{i}}
end
if classifi_index{2}==1
   answer = classifi_index{1}-2 - (classifi_index{3}-2)
else
   answer = classifi_index{1}-2 + (classifi_index{3}-2)
end
%% Plot some features and their hmm counterparts
amount_shown =5;
for i = 7
    figure
    for j = 1:amount_shown
        
        subplot(4, amount_shown, j);
        length = size(extracted_database{i}{j},2);
        plot(1:length, extracted_database{i}{j}(1,:));
        axis([1 50 -.1 1.1])
        title('Ext. dist.')
        set(gca,'xtick',[],'ytick',[])
        
        subplot(4, amount_shown, amount_shown+j);
        plot(1:length, extracted_database{i}{j}(2,:));
        axis([ 1 50 -200 200])
        title('Ext. ang.')
        set(gca,'xtick',[],'ytick',[])
        
        subplot(4, amount_shown, 2*amount_shown+j);
        hmm_seq = rand(hmm(i),length);
        hmm_seq_length = size(hmm_seq,2);
        plot(1:hmm_seq_length, hmm_seq(1,:));
        title('HMM dist.')
        axis([1 50 -.1 1.1])
        set(gca,'xtick',[],'ytick',[])
        
        subplot(4, amount_shown, 3*amount_shown+j);
        hmm_seq = rand(hmm(i),length);
        hmm_seq_length = size(hmm_seq,2);
        plot(1:hmm_seq_length, hmm_seq(2,:));
        title('HMM ang.')
         axis([ 1 50 -200 200])
        set(gca,'xtick',[],'ytick',[])
    end
    suptitle(classes(i))
end

