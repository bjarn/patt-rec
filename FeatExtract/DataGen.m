% DO NOT RUN
clear all;
close all;
time = 4;
num_symbols = 10;
num_symbols = 1;
num_repeats = 20;
characters ={'m'; '+' ; '1'; '2'; '3'; '4'; '5'; '6'; '7'; '8'};


raw_data = cell(num_repeats, 1);
extracted_data = cell(num_repeats, 2);
for i = 1:num_symbols
    disp(characters{i});
    for j = 1:num_repeats
        raw_data{j} =  DrawCharacter(time);
        [extracted_data{j, 1}, extracted_data{j, 2}] = ExtractFeaturesDistAng(raw_data{j});
    end
    %Avoid segmentation fault
    save(characters{i}, 'raw_data', 'extracted_data' );
    clear raw_data;
    clear extracted_data;
end

%% Train single character
clear all;
close all;
time = 4;
num_repeats = 20;
character ='1';
raw_data = cell(num_repeats, 1);
extracted_data = cell(num_repeats, 2);
disp(character);
for j = 1:num_repeats
    raw_data{j} =  DrawCharacter(time);
    [extracted_data{j, 1}, extracted_data{j, 2}] = ExtractFeaturesDistAng(raw_data{j});
end
%Avoid segmentation fault
save(character, 'raw_data', 'extracted_data' );
clear raw_data;
clear extracted_data;

%% Load database
clear all;
close all;
characters ={'m'; '+'; '1'; '2'; '3'; '4'; '5'; '6'; '7'; '8'};
num_symbols = 10;
raw_database = cell(num_symbols, 1);
extracted_database = cell(num_symbols, 1);
for i = 1:num_symbols
    load(characters{i},'raw_data', 'extracted_data');
    raw_database{i} = raw_data;
    extracted_database{i} = extracted_data;
end

%% Extract features from saved data
clear all;
close all;
characters ={'m'; '+'; '1'; '2'; '3'; '4'; '5'; '6'; '7'; '8'};
num_symbols = 10;
num_repeats = 20;
raw_database = cell(num_symbols, 1);
for i = 1:num_symbols
    load(characters{i},'raw_data');
    raw_database{i} = raw_data;
    for j = 1:num_repeats
        [extracted_data{j, 1}, ~] = ExtractFeaturesDistAng(raw_data{j});
    end
    save(characters{i}, 'raw_data', 'extracted_data' );
end

%% Plot characters

for i = 1:num_symbols
    DisplayCharacter(raw_database{i}{1});
end

