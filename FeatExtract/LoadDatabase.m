function [ raw_database, extracted_database] = LoadDatabase()
clear all;
close all;
characters ={'m'; '+'; '1'; '2'; '3'; '4'; '5'; '6'; '7'; '8'};
num_symbols = 10;
raw_database = cell(num_symbols, 1);
extracted_database = cell(num_symbols, 1);
for i = 1:num_symbols
    load(characters{i},'raw_data', 'extracted_data');
    raw_database{i} = raw_data;
    extracted_database{i} = extracted_data;
end
end

