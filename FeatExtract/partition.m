function [training_set, test_set] = partition( extracted_database, index_classes, training_indexes, test_indexes)
    %Generating test and training set
    test_set = cell(1,1);
    for class_index = index_classes
        curr_data = extracted_database{class_index};
        i=1;
        for sample_index = training_indexes
            training_set{class_index, i} = curr_data{sample_index,1}(1:2,:);
            i=i+1;
        end
        i=1;
        for sample_index = test_indexes          
            test_set{class_index, i} = curr_data{sample_index,1}(1:2,:);
            i=i+1;
        end
    end
end

