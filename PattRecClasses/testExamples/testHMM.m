function [ output_args ] = testHMM()
%TESTHMM Summary of this function goes here
%   Detailed explanation goes here
clear all;

%Create test HMM finite
A=[ 0.9 0.1 0;0 0.9 0.1;];
p0=[1 0];
mc=MarkovChain(p0,A);
B=[GaussD('Mean',0, 'StDev', 1),GaussD('Mean',3, 'StDev', 2)];
hmm = HMM(mc,B);

%Observed sequence
x = [-0.2, 2.6, 1.3];
%Conditional prob seq
[px, logs] = prob(hmm.OutputDistr, x);

%Verify forward algo
alfaHatVer =[1.0000 0.3847 0.4189;0 0.6153 0.5811;];
c_ver = [1, 0.1625, 0.8266, 0.0581];
[alfaHat, c] = forward(hmm.StateGen, px);
c_err = c_ver - c
alfaHat_err = alfaHatVer - alfaHat

%Observed output backward algorithm
betaHatVer = [1.0003 1.0393 0; 8.4182 9.3536 2.0822]
betaHat = backward(hmm.StateGen, px, c_ver);
betaHat_err = betaHatVer - betaHat


%Create test left-right HMM infinite
A=[ 0.3 0.7 0;0 0.5 0.5; 0 0 1];
p0=[1 0 0];
mc=MarkovChain(p0,A);
B = [DiscreteD([1 0 0 0]); DiscreteD([0 0.5 0.4 0.1]); DiscreteD([0.1 0.1 0.2 0.6])];
hmm = HMM(mc,B);

%Observed sequence
x = [1, 2, 4, 4, 1];
%Conditional prob seq
[px, logs] = prob(hmm.OutputDistr, x);

%Verify forward algo
alfaHatVer =[1.0000 0 0 0 0; 0 1 1/7 1/79 0; 0 0 6/7 78/79 1;]
c_ver = [1, 0.35, 0.35, 79/140 0.0994];
[alfaHat, c] = forward(hmm.StateGen, px)
c_err = c_ver - c
alfaHat_err = alfaHatVer - alfaHat
end

