%% Verify the Implementation
clear all;
close all;
q = [1; 0];
A = [0.9 0.1 0; 0 0.9 0.1];
b1 = GaussD('Mean', 0, 'StDev', 1);
b2 = GaussD('Mean', 3, 'StDev', 2);
B = [b1; b2];
mc = MarkovChain(q, A);
hmm = HMM(mc, B);

x = [-0.2, 2.6 1.3];
c = [1 0.1626 0.8266 0.0581];

pX = hmm.OutputDistr.prob(x);
betaHat = backward(mc, pX, c)
