
function [angles, zero_sep_angles]= ExtractFeaturesDistAng(raw_data)
    %Calculate angles and dist from center
    data = raw_data(:,find(raw_data(3,:)==1));
    zer_sep_data = zero_separated_data(raw_data);
    
    angles  = distances_angles(data);
    zero_sep_angles = distances_angles(zer_sep_data)
    
end
function distances = distances_angles(raw_data)
    data = normalize_sequence_length(raw_data);
    x_min = min(data(1,:));
    x_max = max(data(1,:));
    
    y_min = min(data(2,:));
    y_max = max(data(2,:));
    
    %Center of circle
    x_c = (x_min + x_max)/2;
    y_c = (y_min + y_max)/2;
    v_c = [x_c;y_c]
    %Distances from center
    distances = sqrt((data(1,:)-x_c).^2 +(data(2,:)-y_c).^2);
    %Normalize
    distances = (distances -min(distances)) / (max(distances)-min(distances));
    %Successive angle movement compared to first pixel
    angles = zeros(1,size(data,2))
    ref_angle = data(1:2,1)-v_c
    for i=1:size(data,2)
        suc_angle=data(1:2,i)-v_c;
        angles(i)=atan2d(ref_angle(1)*suc_angle(2)-ref_angle(2)*suc_angle(1)...
                        ,ref_angle(1)*suc_angle(1)+ref_angle(2)*suc_angle(2));
    end
    angles = (angles -min(angles)) / (max(angles)-min(angles));
    distances = [distances; angles; data(3,:)];
end

function norm_seq = normalize_sequence_length(raw_data)
    n = size(raw_data,2);
    new_length = 50;
    if(n<new_length)
        raw_data = repelem(raw_data, 1 , ceil(new_length/n));
        n = size(raw_data,2);
    end
    sample_indexes = (1:new_length)*floor(n/new_length)
    norm_seq = raw_data(:,sample_indexes);
end

function extracted_data = zero_separated_data(raw_data)
    %  I suppose it needs to distinghush between strokes
    nodraw_ind = find(raw_data(3,:)==0);
    draw_ind = find(raw_data(3,:)==1);

    % Indexes for mouse between strokes
    nodraw_ind = nodraw_ind(find(nodraw_ind > draw_ind(1)))
    nodraw_ind = nodraw_ind(find(nodraw_ind < draw_ind(end)))

    %Remove uneccesary indexes(this got out of hand), indexes that
    %represent mouse movements between strokes. This is probably only
    %useful for the animation
    j = 1;
    for i = 2:size(nodraw_ind,2)
        if(nodraw_ind(i)~=nodraw_ind(i-1)+1)
            nodraw_ind(j+1:i-1)=0;
            j=i;
        end
        %End condition
        if i == size(nodraw_ind,2)
            nodraw_ind(j+1:i)=0;
        end
    end
    nodraw_ind=nodraw_ind(find(nodraw_ind~=0))
    
    % Create complete index-vector and extract data
    ind = [draw_ind, nodraw_ind];
    ind = sort(ind);
    extracted_data = raw_data(:,ind);
end

