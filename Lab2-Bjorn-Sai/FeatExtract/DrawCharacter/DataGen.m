clear all;
number_chars = 2;
time = 5;

raw_data = cell(number_chars,1);
extracted_data = cell(number_chars,1);
zero_sep_extracted_data = cell(number_chars,1);
strokes = cell(number_chars,1);
zero_sep_angles = cell(number_chars,1);
dist_angles = cell(number_chars,1);
zero_sep_dist_angles = cell(number_chars,1);
for i = 1: number_chars
    raw_data{i} = DrawCharacter(time);
    [extracted_data{i}, zero_sep_extracted_data{i}, strokes{i}]= ExtractFeatures2D(raw_data{i});
    [dist_angles{i}, zero_sep_dist_angles]= ExtractFeaturesDistAng(raw_data{i});
end
for i = 1: number_chars
    DisplayCharacter(zero_sep_extracted_data{i})
end

%%
extracted_data = cell(number_chars,1);
zero_sep_extracted_data = cell(number_chars,1);
strokes = cell(number_chars,1);
zero_sep_angles = cell(number_chars,1);
dist_angles = cell(number_chars,1);
zero_sep_dist_angles = cell(number_chars,1);

for i = 1: number_chars
    [extracted_data{i}, zero_sep_extracted_data{i}, strokes{i}]= ExtractFeatures2D(raw_data{i});
    [dist_angles{i}, zero_sep_dist_angles]= ExtractFeaturesDistAng(raw_data{i});
end
for i = 1: number_chars
    DisplayCharacter(dist_angles{i})
end

%%
%hold on
close all
for i = 1: number_chars
    DisplayCharacter(raw_data{i})
end
for i = 1:number_chars
    figure(i+2)

    plot(dist_angles{i}(1,:), dist_angles{i}(2,:));
     figure(i+10)
    plot(1-dist_angles{i}(1,:), dist_angles{i}(2,:));
end
%hold off
