
function [extracted_data, zero_sep_data, strokes]= ExtractFeatures2D(raw_data)
    %Preserves stroke information (is this necessary?)
    [strokes, zero_sep_data] = preserve_strokes_preprocess(raw_data);
    
    %Without preserving stroke information
    extracted_data = raw_data(:,find(raw_data(3,:)==1));
    extracted_data = preprocess(extracted_data);
    extracted_data = normalize_sequence_length(extracted_data);
    
    
end

function norm_seq = normalize_sequence_length(raw_data)
    n = size(raw_data,2);
    new_length = 50;
    if(n<new_length)
        raw_data = repelem(raw_data, 1 , ceil(new_length/n));
        n = size(raw_data,2);
    end
    sample_indexes = (1:new_length)*floor(n/new_length)
    norm_seq = raw_data(:,sample_indexes);
end

%Rescales data
function rescaled=preprocess(extracted_data)
    
    %Make sure a nondraw element doesn't influence
    draw_ind=extracted_data(3,:)==1;
    
    %Normalize scale (deforms simple structures heavily)
    x_norm = (extracted_data(1,:)-min(extracted_data(1,draw_ind)));
    x_norm = x_norm/(max(x_norm(1, draw_ind)-min(extracted_data(2,draw_ind))));
    y_norm = (extracted_data(2,:)-min(extracted_data(2,draw_ind)));
    y_norm = y_norm /(max(y_norm(1, draw_ind)-min(extracted_data(2,draw_ind))));
   
    rescaled= [x_norm; y_norm; extracted_data(3,:)];
    
    %Quantizes so the outcome becomes discrete. 0.05 effectively creates a
    %21*21 img
    %rescaled = round(rescaled/0.05)*0.05;
end

%Divides into an array for each stroke
function [strokes, extracted_data] = preserve_strokes_preprocess(raw_data)
    %Rescale before reshaping into cell-arrays
    extracted_data = preprocess(zero_separated_data(raw_data));
    
    % Generates cellarray-element for each stroke
    ind=find(extracted_data(3,:) == 0);
    %Multiple strokes
    if(numel(ind)>0)
        strokes = cell(numel(ind)+1,1);
        %First stroke
        strokes{1} = extracted_data(:,1:ind(1)-1);
        
        %Middle strokes
        for i = 2:numel(ind)
            strokes{i} = extracted_data(:,ind(i-1)+1:ind(i)-1);
        end
        
        %Last stroke
        strokes{end} = extracted_data(:,ind(end)+1:end);
    %Only one stroke
    else
        strokes = cell(1,1);
        strokes{1} = extracted_data(1:2,:);
    end
end

function extracted_data = zero_separated_data(raw_data)
    %  I suppose it needs to distinghush between strokes
    nodraw_ind = find(raw_data(3,:)==0);
    draw_ind = find(raw_data(3,:)==1);

    % Indexes for mouse between strokes
    nodraw_ind = nodraw_ind(find(nodraw_ind > draw_ind(1)))
    nodraw_ind = nodraw_ind(find(nodraw_ind < draw_ind(end)))

    %Remove uneccesary indexes(this got out of hand), indexes that
    %represent mouse movements between strokes. This is probably only
    %useful for the animation
    j = 1;
    for i = 2:size(nodraw_ind,2)
        if(nodraw_ind(i)~=nodraw_ind(i-1)+1)
            nodraw_ind(j+1:i-1)=0;
            j=i;
        end
        %End condition
        if i == size(nodraw_ind,2)
            nodraw_ind(j+1:i)=0;
        end
    end
    nodraw_ind=nodraw_ind(find(nodraw_ind~=0))
    
    % Create complete index-vector and extract data
    ind = [draw_ind, nodraw_ind];
    ind = sort(ind);
    
    %Rescale before reshaping into cell-arrays
    extracted_data = raw_data(:,ind);
end
