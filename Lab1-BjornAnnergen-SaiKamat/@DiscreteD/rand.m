function R=rand(pD,nData)
%R=rand(pD,nData) returns random scalars drawn from given Discrete Distribution.
%
%Input:
%pD=    DiscreteD object
%nData= scalar defining number of wanted random data elements
%
%Result:
%R= row vector with integer random data drawn from the DiscreteD object pD
%   (size(R)= [1, nData]
%
%----------------------------------------------------
%Code Authors:
%----------------------------------------------------

if numel(pD)>1
    error('Method works only for a single DiscreteD object');
end;

%*** Insert your own code here and remove the following error message

%Uniformly drawn number
r=rand(nData,1);
%Create buckets for 1...nData with sizes proportional to pMass
normalizedBuckets=cumsum([0; pD.ProbMass]);
%Calculate R.V. by checking how many buckets start above the randomly drawn
%number. Apply this to pD.ProbMass each element of array (arrayfun)
R = arrayfun(@(x) sum(x >= normalizedBuckets), r).';

%Check correct
%pD.ProbMass
%test=accumarray(R(:),1)./numel(R);
%test = test./sum(test)

