function S=rand(mc,T)
%S=rand(mc,T) returns a random state sequence from given MarkovChain object.
%
%Input:
%mc=    a single MarkovChain object
%T= scalar defining maximum length of desired state sequence.
%   An infinite-duration MarkovChain always generates sequence of length=T
%   A finite-duration MarkovChain may return shorter sequence,
%   if END state was reached before T samples.
%
%Result:
%S= integer row vector with random state sequence,
%   NOT INCLUDING the END state,
%   even if encountered within T samples
%If mc has INFINITE duration,
%   length(S) == T
%If mc has FINITE duration,
%   length(S) <= T
%
%---------------------------------------------
%Code Authors:
%---------------------------------------------

S=zeros(1,T);%space for resulting row vector

%Initial state
p0=DiscreteD(mc.InitialProb);
S(1)=rand(p0,1);

P = DiscreteD(mc.TransitionProb);

for t=1:T-1
    %Check if reached endstate
    if S(t)>size(P,1)
        S(t)=0;
        break;
    end
    
    %Generate new state
    S(t+1) = rand(P(S(t)),1);
end

%Remove all zeros
S(S==0)=[];
%error('Method not yet implemented');
%continue code from here, and erase the error message........