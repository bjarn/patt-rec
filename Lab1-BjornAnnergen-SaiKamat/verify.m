%Verify first HMM
clear all;

samples=1000

A=[0.99 0.01; 0.03 0.97];
p0=[0.75 0.25];
mc=MarkovChain(p0,A);
B=[GaussD('Mean',0, 'StDev', 1),GaussD('Mean',3, 'StDev', 2)];
hmm = HMM(mc,B);

%Relative freq.
test = rand(mc, samples);
freqState1 = sum(test==1)/samples
freqState2 = sum(test==2)/samples

[testObserved, testStates] = rand(hmm, samples);
mean=mean(testObserved)
var=var(testObserved)

plot(1:500,testObserved, 1:500,zeros(500,1),'--', 1:500,3*ones(500,1),'--')
title('Observed X_t as a function of t')
ylabel('X_t')
xlabel('t')
legend('X_t','\mu_1', '\mu_2')

%%
% Verify second HMM
clear all
samples=1000
A=[0.99 0.01; 0.03 0.97];
p0=[0.75 0.25];
mc=MarkovChain(p0,A);
B2=[GaussD('Mean',0, 'StDev', 1),GaussD('Mean',0, 'StDev', 2)];
hmm2 = HMM(mc,B2);


[testObserved, testStates] = rand(hmm2, samples);

plot(1:500,testObserved, 1:500,zeros(500,1),'--')
title('Observed X_t as a function of t where the Gaussian output distributions both have the \mu=0')
ylabel('X_t')
xlabel('t')
legend('X_t','\mu')
%%
%Finite duration HMM
clear all
close all
samples=10000;
A=[0.8 0.1 0.1;  0.1 0.8 0.1];
p0=[0.75 0.25];
mc=MarkovChain(p0,A);
B2=[GaussD('Mean',0, 'StDev', 1),GaussD('Mean',3, 'StDev', 1)];
hmm2 = HMM(mc,B2);
length = zeros(10000,1);
for i = 1:10000
    [testObserved, testStates] = rand(hmm2, samples);
    length(i)=size(testObserved,2);
end
avglen = mean(length)
%%
%Verify Multi gaussian output distrib

clear all
close all
samples=500
A=[0.99 0.01; 0.03 0.97];
p0=[0.75 0.25];
mc=MarkovChain(p0,A);

B2=[GaussD('Mean',[0 3], 'Covariance', [2 1; 1 4]) GaussD('Mean',[4 6], 'Covariance', [2 1; 1 4])];

hmm2 = HMM(mc,B2);
[testObserved, testStates] = rand(hmm2, samples);

scatter(testObserved(1, testStates==1), testObserved(2,testStates==1)) 
hold
scatter(testObserved(1, testStates==2), testObserved(2,testStates==2))
title('X_t from 2 different states')
ylabel('X_t_y')
xlabel('X_t_x')
legend('S=1','S=2')


