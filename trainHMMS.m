function hmm = trainHMMS(training_set, index_classes, varargin)
    switch nargin 
        case 3
            %Create left right hmm for each class
            for class_index = index_classes
               obsData = cell2mat(training_set(class_index,:));
               lData = arrayfun(@(x) size(x{1},2), training_set(class_index,:));

               gmm = MakeGMM(varargin{1}, obsData);
               hmm(class_index)=MakeLeftRightHMM(varargin{1}, gmm ,obsData,lData);
            end
        case 2
            nStates =[3,4,3,4,4,5,5,5,4,5];
            %nStates = ones(10,1)*3;
            for class_index = index_classes
               obsData = cell2mat(training_set(class_index,:));
               lData = arrayfun(@(x) size(x{1},2), training_set(class_index,:));
%                gmm = MakeGMM(nStates(class_index), obsData);
               mean_class=mean(obsData,2);
               cov_class = cov(obsData');
               std_class = std(obsData');
               pDgen=GaussD('Mean',mean_class,'Covariance',cov_class);
%                pDgen=GaussD('Mean',mean_class,'StDev',std_class);
               hmm(class_index)=MakeLeftRightHMM(nStates(class_index), pDgen ,obsData,lData);
            end
    end
end

